/* NAME//TODO
 * By: John Jekel
 *
 * TODO description
 *
*/

/* Imports */

use std::ops::IndexMut;
use std::ops::Index;

/* Constants */

//TODO

/* Macros */

//TODO (also pub(crate) use the_macro statements here too)

/* Static Variables */

//TODO

/* Types */

#[derive(Clone)]
pub struct Mandelbrot<const MAX_ITERATIONS: usize> {
    x_pixels: usize,
    y_pixels: usize,
    min_real: f64,
    min_imag: f64,
    max_real: f64,
    max_imag: f64,
    iterations: Vec::<usize>//For cheap resizing in case the user changes x_pixels or y_pixels
}

/* Associated Functions and Methods */

impl<const MAX_ITERATIONS: usize> Mandelbrot<MAX_ITERATIONS> {
    pub fn new(x_pixels: usize, y_pixels: usize, min_real: f64, min_imag: f64, max_real: f64, max_imag: f64) -> Mandelbrot::<MAX_ITERATIONS> {
        assert!(MAX_ITERATIONS > 0);//TODO do this at compile time instead
        assert!(x_pixels != 0, "x_pixels must be non-zero");
        assert!(y_pixels != 0, "y_pixels must be non-zero");
        assert!(min_real < max_real, "min_real must be < max_real");
        assert!(min_imag < max_imag, "min_imag must be < max_imag");

        //println!("{}", x_pixels * y_pixels);

        let mut new_iterations_vec = Vec::<usize>::with_capacity(x_pixels * y_pixels);//Also sanity checks x and y pixels
        new_iterations_vec.resize(x_pixels * y_pixels, 0);

        let mut new_mandelbrot = Mandelbrot {
            x_pixels: x_pixels,
            y_pixels: y_pixels,
            min_real: min_real,
            min_imag: min_imag,
            max_real: max_real,
            max_imag: max_imag,
            iterations: new_iterations_vec
        };
        new_mandelbrot.update_iterations();
        return new_mandelbrot;
    }

    pub fn set_x_pixels(self: &mut Self, x_pixels: usize) {
        if self.x_pixels != x_pixels {
            assert!(x_pixels != 0, "x_pixels must be non-zero");
            self.x_pixels = x_pixels;
            self.update_iterations();
        }
    }

    pub fn set_y_pixels(self: &mut Self, y_pixels: usize) {
        if self.y_pixels != y_pixels {
            assert!(y_pixels != 0, "x_pixels must be non-zero");
            self.y_pixels = y_pixels;
            self.update_iterations();
        }
    }

    pub fn set_min_real(self: &mut Self, min_real: f64) {
        if self.min_real != min_real {
            assert!(min_real < self.max_real, "min_real must be < max_real");
            self.min_real = min_real;
            self.update_iterations();
        }
    }

    pub fn set_max_real(self: &mut Self, max_real: f64) {
        if self.max_real != max_real {
            assert!(self.min_real < max_real, "min_real must be < max_real");
            self.max_real = max_real;
            self.update_iterations();
        }
    }

    pub fn set_min_imag(self: &mut Self, min_imag: f64) {
        if self.min_imag != min_imag {
            assert!(min_imag < self.max_imag, "min_imag must be < max_imag");
            self.min_imag = min_imag;
            self.update_iterations();
        }
    }

    pub fn set_max_imag(self: &mut Self, max_imag: f64) {
        if self.max_imag != max_imag {
            assert!(self.min_imag < max_imag, "min_imag must be < max_imag");
            self.max_imag = max_imag;
            self.update_iterations();
        }
    }

    fn set_rez(self: &mut Self, x_pixels: usize, y_pixels: usize) {
        todo!();
    }

    pub fn iterations_ref(self: &Self) -> &[usize] {
        return &self.iterations;
    }

    fn update_iterations(self: &mut Self) {
        let real_length: f64 = self.max_real - self.min_real;
        let real_step_amount: f64 = real_length / (self.x_pixels as f64);
        let imag_length: f64 = self.max_imag - self.min_imag;
        let imag_step_amount: f64 = imag_length / (self.y_pixels as f64);
        //println!("update_iterations: {} {} {} {}", real_length, real_step_amount, imag_length, imag_step_amount);

        let mut c_real: f64 = self.min_real;
        for x in 0..self.x_pixels {
            let mut c_imag: f64 = self.min_imag;
            for y in 0..self.y_pixels {
                *self.at(x, y) = Self::mandelbrot_iterations(c_real, c_imag);
                c_imag += imag_step_amount;
            }
            c_real += real_step_amount;
        }
    }

    fn mandelbrot_iterations(c_real: f64, c_imag: f64) -> usize {//Returns MAX_ITERATIONS if it is bounded
        //println!("mandelbrot iteration with params: {} {}", c_real, c_imag);
        let diverge_threshold: f64 = 2.0;

        //z_0 = 0
        let mut z_real: f64 = 0.0;
        let mut z_imag: f64 = 0.0;

        //We exit the loop in two cases: if we reach MAX_ITERATIONS (meaning we assume the c value produces a bounded series)
        //or the modulus of the complex number exceeds the diverge_threshold (meaning the c value produces an unbounded series)
        let mut i: usize = 0;
        while (i < MAX_ITERATIONS) && (((z_real * z_real) + (z_imag * z_imag)) < (diverge_threshold * diverge_threshold)) {
            //println!("iteration {} starts: z_real {}, z_imag {}", i, z_real, z_imag);
            //z_(n+1) = z_n^2 + c
            let next_z_real = (z_real * z_real) - (z_imag * z_imag) + c_real;
            let next_z_imag = (2.0 * z_real * z_imag) + c_imag;
            z_real = next_z_real;
            z_imag = next_z_imag;
            //println!("iteration {} ends: z_real {}, z_imag {}", i, z_real, z_imag);
            i += 1;
        }
        //println!("mandelbrot ends returning {}", i);
        return i;
    }

    #[inline(always)]
    fn at(self: &mut Self, x: usize, y: usize) -> &mut usize {//unchecked for speed in release builds
        debug_assert!(x < self.x_pixels);
        debug_assert!(y < self.y_pixels);
        return self.iterations.index_mut(x + (y * self.x_pixels));
    }
}

/* Functions */

impl<const MAX_ITERATIONS: usize> Index<usize> for Mandelbrot<MAX_ITERATIONS> {
    type Output = usize;

    fn index(self: &Self, index: usize) -> &usize {
        assert!(index < self.iterations.len());
        return self.iterations.index(index);
    }
}

impl<const MAX_ITERATIONS: usize> Index<(usize, usize)> for Mandelbrot<MAX_ITERATIONS> {//x, y indexing
    type Output = usize;

    fn index(self: &Self, index: (usize, usize)) -> &usize {
        assert!(index.0 < self.x_pixels);
        assert!(index.1 < self.y_pixels);
        return self.iterations.index(index.0 + (index.1 * self.x_pixels));
    }
}
