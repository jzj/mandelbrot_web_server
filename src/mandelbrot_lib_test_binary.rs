/* NAME//TODO
 * By: John Jekel
 *
 * TODO description
 *
*/

/* Imports */

use mandelbrot_lib::Mandelbrot;

use std::fs::File;

/* Constants */

//TODO

/* Macros */

//TODO (also pub(crate) use the_macro statements here too)

/* Static Variables */

//TODO

/* Types */

//TODO

/* Associated Functions and Methods */

//TODO

/* Functions */

fn main() {
    eprintln!("Creating new Mandelbrot");
    let mandelbrot = Mandelbrot::<255>::new(75, 30, -2.3, -1.1, 0.8, 1.1);

    for y in 0..30 {
        for x in 0..75 {
            if mandelbrot[(x, y)] == 255 {
                print!("*");
            } else {
                print!(" ");
            }
        }
        println!();
    }

    let handle = std::thread::spawn(|| {
        eprintln!("Creating new Mandelbrot");
        let new_mandelbrot = Mandelbrot::<255>::new(1920, 1080, -2.3, -1.1, 0.8, 1.1);

        let mut tga_image_data = Vec::<u8>::with_capacity(1920 * 1080 * 3);
        for y in 0..1080 {
            for x in 0..1920 {
                let value = new_mandelbrot[(x, y)] / 1;
                tga_image_data.push(new_mandelbrot[(x, y)] as u8);
                tga_image_data.push(new_mandelbrot[(x, y)] as u8);
                tga_image_data.push(new_mandelbrot[(x, y)] as u8);
            }
        }

        let tga_file_vec = create_tga_vec(1920, 1080, 24, &tga_image_data);
        std::fs::write("test_image2.tga", &tga_file_vec);
    });

    /*eprintln!("Creating new Mandelbrot");
    let mandelbrot3 = Mandelbrot::<255>::new(1920, 1080, -2.3, -1.1, 0.8, 1.1);//Pass 1

    let mut num_iterations = [0usize; 256];//Pass 2
    for y in 0..1080 {
        for x in 0..1920 {
            num_iterations[mandelbrot3[(x, y)]] += 1;//Count up the number of each iteration count
        }
    }
    let total: usize = num_iterations.iter().sum();//Pass 3


    let mut tga_image_data = Vec::<u8>::with_capacity(1920 * 1080 * 3);
    for y in 0..1080 {
        for x in 0..1920 {
            let value = mandelbrot3[(x, y)];
            tga_image_data.push(mandelbrot3[(x, y)] as u8);
            tga_image_data.push(mandelbrot3[(x, y)] as u8);
            tga_image_data.push(mandelbrot3[(x, y)] as u8);
        }
    }

    let tga_file_vec = create_tga_vec(1920, 1080, 24, &tga_image_data);
    std::fs::write("test_image3.tga", &tga_file_vec);
    */

    eprintln!("Creating new Mandelbrot");
    const X_PIXELS5: usize = 3840;
    const Y_PIXELS5: usize = 2160;
    const ITERATIONS5: usize = 1000;
    const ITERATIONS5F: f64 = ITERATIONS5 as f64;
    //let mandelbrot5 = Mandelbrot::<ITERATIONS5>::new(X_PIXELS5, Y_PIXELS5, -2.3, -1.1, 0.8, 1.1);
    let mandelbrot5 = Mandelbrot::<ITERATIONS5>::new(X_PIXELS5, Y_PIXELS5, -0.7473, 0.1112, -0.7433, 0.1147);

    let mut tga_image_data = Vec::<u8>::with_capacity(X_PIXELS5 * Y_PIXELS5 * 3);
    for y in 0..Y_PIXELS5 {
        for x in 0..X_PIXELS5 {
            if mandelbrot5[(x, y)] == ITERATIONS5 {
                tga_image_data.push(0);
                tga_image_data.push(0);
                tga_image_data.push(0);
                continue;
            }

            let i: f64 = mandelbrot5[(x, y)] as f64;
            let v: f64 = ((i / ITERATIONS5F) * 255.0).powf(1.5) % 255.0;
            let v_integer: u32 = v as u32;
            /*tga_image_data.push((v_integer & 0xFF) as u8);
            tga_image_data.push(((v_integer >> 8) & 0xFF) as u8);
            tga_image_data.push(((v_integer >> 16) & 0xFF) as u8);
            */
            tga_image_data.push(((v_integer / 3) * 3) as u8);
            tga_image_data.push((((v_integer / 3) * 3) + 1) as u8);
            tga_image_data.push((((v_integer / 3) * 3) + 2) as u8);

            /*let i: f64 = mandelbrot5[(x, y)] as f64;
            //Create hsv
            let h: f64 = ((i / ITERATIONS5F) * 360.0).powf(1.5) % 360.0;
            let s: f64 = 100.0;
            let v: f64 = (i / ITERATIONS5F) * 100.0;

            //Convert to rgb
            let c: f64 = v * s;
            let x: f64 = c * (1.0 - (((h / 60.0) % 2.0) - 1.0).abs());
            let m: f64 = v - c;

            assert!(h > 0.0);
            let (r_prime, g_prime, b_prime): (f64, f64, f64) =
            if h < 60.0       { (c, x, 0.0) }
            else if h < 120.0 { (x, c, 0.0) }
            else if h < 180.0 { (0.0, c, x) }
            else if h < 240.0 { (0.0, x, c) }
            else if h < 300.0 { (x, 0.0, c) }
            else              { (c, 0.0, x) };

            assert!(r_prime < 1.0);
            assert!(g_prime < 1.0);
            assert!(b_prime < 1.0);

            let r: u8 = ((r_prime + m) * 255.0) as u8;
            let g: u8 = ((g_prime + m) * 255.0) as u8;
            let b: u8 = ((b_prime + m) * 255.0) as u8;

            tga_image_data.push(r);
            tga_image_data.push(g);
            tga_image_data.push(b);
            */
        }
    }
    let tga_file_vec = create_tga_vec(X_PIXELS5.try_into().unwrap(), Y_PIXELS5.try_into().unwrap(), 24, &tga_image_data);
    std::fs::write("test_image5.tga", &tga_file_vec);

    handle.join().unwrap();
}

fn create_tga_vec(x_pixels: u16, y_pixels: u16, bpp: u8, image_data: &[u8]) -> Vec::<u8> {
    let mut new_vec = Vec::<u8>::with_capacity(image_data.len() + 18);
    new_vec.push(0u8);//ID Length (unused)
    new_vec.push(0u8);//Colour map type (no colour map)
    new_vec.push(2u8);//Image type (uncompressed true-colour)

    //Colour map (5 bytes, unused)
    new_vec.push(0u8);//First entry index low byte (unused)
    new_vec.push(0u8);//First entry index high byte (unused)
    new_vec.push(0u8);//Colour map length low byte (unused)
    new_vec.push(0u8);//Colour map length high byte (unused)
    new_vec.push(0u8);//Colour map entry size (unused)

    //Image specification (10 bytes)
    new_vec.push(0u8);//X origin low byte (coordinates of lower left corner of image)
    new_vec.push(0u8);//X origin high byte (coordinates of lower left corner of image)
    new_vec.push(0u8);//Y origin low byte (coordinates of lower left corner of image)
    new_vec.push(0u8);//Y origin high byte (coordinates of lower left corner of image)
    new_vec.push((x_pixels & 0xFF) as u8);       //Image width low byte
    new_vec.push(((x_pixels >> 8) & 0xFF) as u8);//Image width high byte
    new_vec.push((y_pixels & 0xFF) as u8);       //Image height low byte
    new_vec.push(((y_pixels >> 8) & 0xFF) as u8);//Image height high byte
    new_vec.push(bpp);//Pixel depth
    new_vec.push(0u8);//Image descriptor

    new_vec.extend_from_slice(image_data);
    return new_vec;
}
