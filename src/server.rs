/* NAME//TODO
 * By: John Jekel
 *
 * TODO description
 *
*/

/* Imports */

use mandelbrot_lib::Mandelbrot;
use std::net::TcpListener;
use std::net::TcpStream;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Write;
use std::thread;
use std::thread::available_parallelism;
use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::time::Instant;

/* Constants */

const BIND_STRING: &str = "0.0.0.0:8080";

const HTML_404: &str = "
    <!DOCTYPE html>
    <html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <title>404 Not Found</title>
    </head>
    <body>
        <h1>Error: 404 Not Found</h1>
        <p>Powered by the Jekel Mandelbrot Web Server</p>
    </body>
    </html>
";

const INDEX: &str = "
    <!DOCTYPE html>
    <html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <title>Mandelbrot Web Server</title>
    </head>
    <body>
        <h1>Generate a Mandelbrot Image!</h1>
        <p>TODO allow input to redirect to a url that will provide an image</p>
        <p >For now, <a href=\"/generate?x_pixels=150&y_pixels=60&min_real=-2.3&min_imag=-1.1&max_real=0.8&max_imag=1.1\">visit here to test</a></p>
    </body>
    </html>
";

/* Macros */

//TODO (also pub(crate) use the_macro statements here too)

/* Static Variables */

//TODO

/* Types */

//TODO

/* Associated Functions and Methods */

//TODO

/* Functions */

//TODO reduce logging overhead

fn main() {
    eprintln!("Starting server...");
    let threads: usize = available_parallelism().unwrap().get();
    println!("{} threads are available for use", threads);//There are that many stream processing threads, plus one logging thread

    //Create a thread for logging//TODO what about graceful shutdown
    eprintln!("Launching logging thread...");
    let (log_sender, log_receiver) = mpsc::channel::<String>();
    thread::spawn(move || { log_thread(log_receiver); });

    //If None is set, that is a shutdown request, otherwise it means there is a stream to process
    let mut sender_vector = Vec::<Sender<Option<TcpStream>>>::with_capacity(threads);

    //Create threads
    for id in 0..threads {
        log_sender.send(format!("Launching thread {}\n", id)).unwrap();
        let (tx, rx) = mpsc::channel::<Option<TcpStream>>();
        sender_vector.push(tx);

        let log_sender_copy = log_sender.clone();

        //TODO keep going handles for graceful shutdown
        thread::spawn(move || { stream_thread(id, rx, log_sender_copy); });
    }

    //Distribute requests in round-robin fashion to threads//TODO what about graceful shutdown
    let listener = TcpListener::bind(BIND_STRING).unwrap();
    let mut current_id: usize = 0;
    for stream in listener.incoming() {
        sender_vector[current_id].send(Some(stream.unwrap())).unwrap();
        current_id += 1;
        if current_id >= threads {
            current_id = 0;
        }
    }
}

fn log_thread(reciever: Receiver<String>) {
    eprintln!("Started logging thread.");
    loop {
        if let Ok(string) = reciever.recv() {
            eprint!("{}", string);
        } else {
            return;
        }
    }
    //eprintln!("Stoping logging thread.");
}

fn stream_thread(thread_id: usize, reciever: Receiver<Option<TcpStream>>, log_sender: Sender<String>) {
    log_sender.send(format!("Started thread with id {}.\n", thread_id)).unwrap();
    loop {
        let message = reciever.recv();

        let processing_start_time = Instant::now();

        match message {
            Ok(Some(mut stream)) => {
                log_sender.send(format!("Thread with id {} got a connection/stream to process ({:?}).\n", thread_id, stream.peer_addr())).unwrap();
                let line_vector: Vec::<String> = BufReader::new(&mut stream).lines().map(|result| result.unwrap()).take_while(|line| !line.is_empty()).collect();

                if line_vector.len() != 0 {

                    //Print the request to the console
                    let mut request_as_one_giant_string = String::new();
                    for line in line_vector.iter() {
                        request_as_one_giant_string.push_str(&line);
                        request_as_one_giant_string.push('\n');
                    }
                    log_sender.send(format!("Thread with id {} got a request: \n{}\n", thread_id, request_as_one_giant_string)).unwrap();

                    let mut first_line_iterator = line_vector[0].split(' ');
                    match first_line_iterator.next() {
                        Some("GET") => {
                            if let Some(path) = first_line_iterator.next() {
                                log_sender.send(format!("Thread with id {} is processing a GET with path \"{}\"\n", thread_id, path)).unwrap();
                                //TODO avoid overhead of String
                                let mut status_line = String::new();
                                let mut contents = String::new();
                                handle_get(&path, &mut status_line, &mut contents);
                                stream.write_all(format!("{}\r\nContent-Length: {}\r\n\r\n{}", status_line, contents.len(), contents).as_bytes()).unwrap();
                            } else {
                                log_sender.send(format!("Thread with id {} got an invalid request.\n", thread_id)).unwrap();
                            }
                        }
                        _ => { log_sender.send(format!("Thread with id {} got an invalid request.\n", thread_id)).unwrap(); }
                    }
                }
            },
            _ => { break; }
        }

        log_sender.send(format!("Thread with id {} took {}us to process the last request\n", thread_id, processing_start_time.elapsed().as_micros())).unwrap();
    }
    log_sender.send(format!("Stopping thread with id {}.\n", thread_id)).unwrap();
}

fn handle_get(path: &str, status_line: &mut String, contents: &mut String) {
    if (path == "/") || (path == "/index.html") {
        status_line.push_str("HTTP/1.1 200 OK");
        contents.push_str(INDEX);
    } else if path.starts_with("/generate") {
        if let Some((x_pixels, y_pixels, min_real, min_imag, max_real, max_imag)) = parse_mandelbrot_generate(path) {
            status_line.push_str("HTTP/1.1 200 OK");
            //TODO log what is happening

            let mandelbrot = Mandelbrot::<10000>::new(x_pixels, y_pixels, min_real, min_imag, max_real, max_imag);
            for y in 0..y_pixels {
                for x in 0..x_pixels {
                    if mandelbrot[(x, y)] == 10000 {
                        contents.push('*');
                    } else {
                        contents.push(' ');
                    }
                }
                contents.push('\n');
            }
            //TODO actually use the parsed values
            //status_line.push_str("HTTP/1.1 200 OK");
            //contents.push_str(&format!("{:?}", (x_pixels, y_pixels, min_real, min_imag, max_real, max_imag)));
        } else {
            status_line.push_str("HTTP/1.1 404 Not Found");
            contents.push_str(HTML_404);
        }
    } else {
        status_line.push_str("HTTP/1.1 404 Not Found");
        contents.push_str(HTML_404);
    }
}

//Returns x_pixels, y_pixels, min_real, min_imag, max_real, max_imag
fn parse_mandelbrot_generate(path: &str) -> Option<(usize, usize, f64, f64, f64, f64)> {
    let split_url_args: Vec::<&str> = path.split(&['?', '&', '=']).collect();

    //Ensure all of the parameters required are present
    if  split_url_args.len() != 13 ||
        split_url_args[0] != "/generate" ||
        split_url_args[1] != "x_pixels" ||
        split_url_args[3] != "y_pixels" ||
        split_url_args[5] != "min_real" ||
        split_url_args[7] != "min_imag" ||
        split_url_args[9] != "max_real" ||
        split_url_args[11] != "max_imag"
    {
        return None;
    }

    //Attempt to parse everything
    let mut result: (usize, usize, f64, f64, f64, f64) = (0, 0, 0.0, 0.0, 0.0, 0.0);
    result.0 = split_url_args[2].parse::<usize>().ok()?;
    result.1 = split_url_args[4].parse::<usize>().ok()?;
    result.2 = split_url_args[6].parse::<f64>().ok()?;
    result.3 = split_url_args[8].parse::<f64>().ok()?;
    result.4 = split_url_args[10].parse::<f64>().ok()?;
    result.5 = split_url_args[12].parse::<f64>().ok()?;

    //Sanity check the values
    if  result.0 == 0 ||
        result.1 == 0 ||
        result.2 >= result.4 ||
        result.3 >= result.5
    {
        return None;
    }

    return Some(result);
}

/*fn handle_get(path: &str, status_line: &mut String, contents: &mut String) {
    let canonicalized_base_path = std::path::Path::new(DIRECTORY_ROOT_PATH).canonicalize().expect("The base path should be configured correctly.");
    let raw_path_with_base_string = DIRECTORY_ROOT_PATH.to_string() + path;
    let raw_path_with_base = std::path::Path::new(&raw_path_with_base_string);
    let path_to_canonicalize: String;

    if raw_path_with_base.is_dir() {
        path_to_canonicalize = raw_path_with_base_string + "/index.html";
    } else {
        path_to_canonicalize = raw_path_with_base_string;
    }

    if let Ok(canonicalized_path) = std::path::Path::new(&path_to_canonicalize).canonicalize() {//Try to canonicalize the path
        if canonicalized_path.starts_with(canonicalized_base_path) {//Check that the path is a (sub)directory of the root
            if let Ok(mut file) = std::fs::File::open(canonicalized_path) {//Try to open the path as a file (it's possible index.html is a directory)
                if matches!(file.read_to_string(contents), Ok(_)) {//Try to read the file
                    status_line.push_str("HTTP/1.1 200 OK");
                    return;
                } else {
                    contents.truncate(0);//Fallthrough to 403 Forbidden
                }
            }
        }

        status_line.push_str("HTTP/1.1 403 Forbidden");
        contents.push_str(HTML_403);
        return;
    }

    status_line.push_str("HTTP/1.1 404 Not Found");
    contents.push_str(HTML_404);
}
*/
